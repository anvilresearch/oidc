'use strict'

/**
 * Dependencies
 * @ignore
 */
const BaseAlgorithm = require('./BaseAlgorithm')

/**
 * RSASSA-PSS
 */
class RSASSA_PSS extends BaseAlgorithm {}

/**
 * Export
 */
module.exports = RSASSA_PSS
