'use strict'

/**
 * Test dependencies
 */
const cwd = process.cwd()
const path = require('path')
const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')

/**
 * Assertions
 */
chai.use(sinonChai)
chai.should()
let expect = chai.expect

/**
 * Code under test
 */
const AuthenticationRequest = require(path.join(cwd, 'src', 'handlers', 'AuthenticationRequest'))

/**
 * Tests
 */
describe('AuthenticationRequest', () => {

  /**
   * Handle
   */
  describe('handle', () => {})

  /**
   * Constructor
   */
  describe('constructor', () => {
    let params, req, res, host, provider

    before(() => {
      params = { response_type: 'code' }
      req = { method: 'GET', query: params }
      res = {}
      host = {}
      provider = { host }
    })

    it('should set "params" from request query', () => {
      let req = { method: 'GET', query: params }
      let request = new AuthenticationRequest(req, res, provider)
      request.params.should.equal(params)
    })

    it('should set "params" from request body', () => {
      let req = { method: 'GET', query: params }
      let request = new AuthenticationRequest(req, res, provider)
      request.params.should.equal(params)
    })

    it('should set "responseTypes"', () => {
      let req = { method: 'GET', query: { response_type: 'code id_token token' } }
      let request = new AuthenticationRequest(req, res, provider)
      request.responseTypes.should.eql([ 'code', 'id_token', 'token' ])
    })

    it('should set "responseMode" default', () => {
      let req = { method: 'GET', query: params }
      let request = new AuthenticationRequest(req, res, provider)
      request.responseMode.should.eql('?')
    })

    it('should set "responseMode" explicitly', () => {
      let req = {
        method: 'GET',
        query: {
          response_type: 'id_token token',
          response_mode: 'query'
        }
      }

      let request = new AuthenticationRequest(req, res, provider)
      request.responseMode.should.eql('?')
    })
  })

  /**
   * Supported Response Types
   */
  describe('supportedResponseType', () => {
    let res, host, provider

    beforeEach(() => {
      res = {}
      host = {}
      provider = { host, supported_response_types: ['code id_token'] }
    })

    it('should return true with a supported response type parameter', () => {
      let params = { response_type: 'code id_token' }
      let req = { method: 'GET', query: params }
      let request = new AuthenticationRequest(req, res, provider)
      request.supportedResponseType().should.equal(true)
    })

    it('should return false with an unsupported response type parameter', () => {
      let params = { response_type: 'code id_token token' }
      let req = { method: 'GET', query: params }
      let request = new AuthenticationRequest(req, res, provider)
      request.supportedResponseType().should.equal(false)
    })
  })

  /**
   * Supported Response Mode
   */
  describe('supportedResponseMode', () => {
    let res, host, provider

    beforeEach(() => {
      res = {}
      host = {}
      provider = { host, supported_response_modes: ['query', 'fragment'] }
    })

    it('should return true with an undefined response mode parameter', () => {
      let req = { method: 'GET', query: {} }
      let request = new AuthenticationRequest(req, res, provider)
      request.supportedResponseMode().should.equal(true)
    })

    it('should return true with a supported response mode parameter', () => {
      let params = { response_mode: 'fragment' }
      let req = { method: 'GET', query: params }
      let request = new AuthenticationRequest(req, res, provider)
      request.supportedResponseMode().should.equal(true)
    })

    it('should return false with an unsupported response mode parameter', () => {
      let params = { response_mode: 'unsupported' }
      let req = { method: 'GET', query: params }
      let request = new AuthenticationRequest(req, res, provider)
      request.supportedResponseMode().should.equal(false)
    })
  })

  /**
   * Required Nonce Provided
   */
  describe('requiredNonceProvided', () => {
    it('should return true when nonce is not required', () => {
      let req = { method: 'GET', query: { response_type: 'code' } }
      let request = new AuthenticationRequest(req, {}, { host: {} })
      request.requiredNonceProvided().should.equal(true)
    })

    it('should return true when nonce is required and provided', () => {
      let req = {
        method: 'GET',
        query: {
          response_type: 'id_token token',
          nonce: 'n0nc3'
        }
      }

      let request = new AuthenticationRequest(req, {}, { host: {} })
      request.requiredNonceProvided().should.equal(true)
    })

    it('should return false when nonce is required and missing', () => {
      let req = { method: 'GET', query: { response_type: 'id_token token' } }
      let request = new AuthenticationRequest(req, {}, { host: {} })
      request.requiredNonceProvided().should.equal(false)
    })
  })

  /**
   * Validate
   */
  describe('validate', () => {

    describe('with missing client_id parameter', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'forbidden')
        params = {}
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = { host }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.forbidden.restore()
      })

      it('should respond "403 Forbidden"', () => {
        request.forbidden.should.have.been.calledWith({
          error: 'unauthorized_client',
          error_description: 'Missing client id'
        })
      })
    })

    describe('with missing redirect_uri parameter', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'badRequest')
        params = { client_id: 'uuid' }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = { host }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.badRequest.restore()
      })

      it('should respond "400 Bad Request"', () => {
        request.badRequest.should.have.been.calledWith({
          error: 'invalid_request',
          error_description: 'Missing redirect uri'
        })
      })
    })

    describe('with unknown client', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'unauthorized')
        params = { client_id: 'uuid', redirect_uri: 'https://example.com/callback' }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = {
          host,
          getClient: sinon.stub().returns(Promise.resolve(null))
        }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.unauthorized.restore()
      })

      it('should respond "401 Unauthorized', () => {
        request.unauthorized.should.have.been.calledWith({
          error: 'unauthorized_client',
          error_description: 'Unknown client'
        })
      })
    })

    describe('with mismatching redirect uri', () => {
      let params, client, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'badRequest')
        params = { client_id: 'uuid', redirect_uri: 'https://example.com/wrong' }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        client = { redirect_uris: ['https://example.com/callback'] }
        provider = {
          host,
          getClient: sinon.stub().returns(Promise.resolve(client))
        }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.badRequest.restore()
      })

      it('should respond "400 Bad Request"', () => {
        request.badRequest.should.have.been.calledWith({
          error: 'invalid_request',
          error_description: 'Mismatching redirect uri'
        })
      })
    })

    describe('with missing response_type parameter', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'redirect')
        params = { client_id: 'uuid', redirect_uri: 'https://example.com/callback' }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = {
          host,
          getClient: sinon.stub().returns(Promise.resolve({
            redirect_uris: [
              'https://example.com/callback'
            ]
          }))
        }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.redirect.restore()
      })

      it('should respond "302 Redirect"', () => {
        request.redirect.should.have.been.calledWith({
          error: 'invalid_request',
          error_description: 'Missing response type'
        })
      })
    })

    describe('with missing scope parameter', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'redirect')
        params = {
          client_id: 'uuid',
          redirect_uri: 'https://example.com/callback',
          response_type: 'code'
        }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = {
          host,
          getClient: sinon.stub().returns(Promise.resolve({
            redirect_uris: [
              'https://example.com/callback'
            ]
          }))
        }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.redirect.restore()
      })

      it('should respond "302 Redirect"', () => {
        request.redirect.should.have.been.calledWith({
          error: 'invalid_scope',
          error_description: 'Missing scope'
        })
      })
    })

    describe('with missing openid scope value', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'redirect')
        params = {
          client_id: 'uuid',
          redirect_uri: 'https://example.com/callback',
          response_type: 'code',
          scope: 'profile'
        }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = {
          host,
          getClient: sinon.stub().returns(Promise.resolve({
            redirect_uris: [
              'https://example.com/callback'
            ]
          }))
        }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.redirect.restore()
      })

      it('should respond "302 Redirect"', () => {
        request.redirect.should.have.been.calledWith({
          error: 'invalid_scope',
          error_description: 'Missing openid scope'
        })
      })
    })

    describe('with missing required nonce', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'redirect')
        params = {
          client_id: 'uuid',
          redirect_uri: 'https://example.com/callback',
          response_type: 'id_token token',
          scope: 'openid profile'
        }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = {
          host,
          getClient: sinon.stub().returns(Promise.resolve({
            redirect_uris: [
              'https://example.com/callback'
            ]
          }))
        }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.redirect.restore()
      })

      it('should respond "302 Redirect"', () => {
        request.redirect.should.have.been.calledWith({
          error: 'invalid_request',
          error_description: 'Missing nonce'
        })
      })
    })

    describe('with unsupported response type', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'redirect')
        params = {
          client_id: 'uuid',
          redirect_uri: 'https://example.com/callback',
          response_type: 'code unsupported',
          scope: 'openid',
          nonce: 'n0nc3'
        }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = {
          host,
          supported_response_types: ['code', 'id_token token'],
          getClient: sinon.stub().returns(Promise.resolve({
            redirect_uris: [
              'https://example.com/callback'
            ]
          }))
        }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.redirect.restore()
      })

      it('should respond "302 Redirect"', () => {
        request.redirect.should.have.been.calledWith({
          error: 'unsupported_response_type',
          error_description: 'Unsupported response type'
        })
      })
    })

    describe('with unsupported response mode', () => {
      let params, req, res, host, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'redirect')
        params = {
          client_id: 'uuid',
          redirect_uri: 'https://example.com/callback',
          response_type: 'code',
          response_mode: 'unsupported',
          scope: 'openid',
          nonce: 'n0nc3'
        }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        provider = {
          host,
          supported_response_types: ['code', 'id_token token'],
          supported_response_modes: ['query', 'fragment'],
          getClient: sinon.stub().returns(Promise.resolve({
            redirect_uris: [
              'https://example.com/callback'
            ]
          }))
        }
        request = new AuthenticationRequest(req, res, provider)
        request.validate(request)
      })

      after(() => {
        AuthenticationRequest.prototype.redirect.restore()
      })

      it('should respond "302 Redirect"', () => {
        request.redirect.should.have.been.calledWith({
          error: 'unsupported_response_mode',
          error_description: 'Unsupported response mode'
        })
      })
    })

    describe('with valid request', () => {
      let params, client, req, res, host, provider, request, promise

      before(() => {
        params = {
          client_id: 'uuid',
          redirect_uri: 'https://example.com/callback',
          response_type: 'code',
          scope: 'openid',
          nonce: 'n0nc3'
        }
        req = { method: 'GET', query: params }
        res = {}
        host = {}
        client = { redirect_uris: 'https://example.com/callback' }
        provider = {
          host,
          supported_response_types: ['code', 'id_token token'],
          supported_response_modes: ['query', 'fragment'],
          getClient: sinon.stub().returns(Promise.resolve(client))
        }
        request = new AuthenticationRequest(req, res, provider)
        promise = request.validate(request)
      })

      it('should return a promise', () => {
        promise.should.be.instanceof(Promise)
      })

      it('should set client on the request', () => {
        request.client.should.equal(client)
      })
    })
  })

  describe('authorize', () => {
    describe('with consent', () => {
      let params, client, req, res, host, provider, request, promise

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'allow')
        req = { method: 'GET', query: { authorize: true } }
        res = {}
        host = {}
        provider = {
          host,
        }
        request = new AuthenticationRequest(req, res, provider)
        promise = request.authorize(request)
      })

      after(() => {
        AuthenticationRequest.prototype.allow.restore()
      })

      it('should grant access', () => {
        request.allow.should.have.been.called
      })
    })

    describe('without consent', () => {
      let req, res, provider, request

      before(() => {
        sinon.stub(AuthenticationRequest.prototype, 'deny')
        req = { method: 'GET', query: { authorize: false } }
        res = {}
        provider = {
          host: {}
        }
        request = new AuthenticationRequest(req, res, provider)
        request.authorize(request)
      })

      after(() => {
        AuthenticationRequest.prototype.deny.restore()
      })

      it('should deny access', () => {
        request.deny.should.have.been.called
      })
    })
  })

  describe('allow', () => {})

  describe('deny', () => {
    before(() => {
      sinon.stub(AuthenticationRequest.prototype, 'redirect')
      let request = new AuthenticationRequest({}, {}, { host: {} })
      request.deny()
    })

    after(() => {
      AuthenticationRequest.prototype.redirect.restore()
    })

    it('should respond "302 Redirect"', () => {
      AuthenticationRequest.prototype.redirect.should.have.been.calledWith({
        error: 'access_denied'
      })
    })
  })

  describe('includeAccessToken', () => {})
  describe('includeAuthorizationCode', () => {})
  describe('includeIDToken', () => {})
  describe('includeSessionState', () => {})
})
