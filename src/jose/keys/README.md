# JSON Web Keychain

Scope of implementation

- [ ] KeyChain supporting arbitrary organization of keys
- [ ] Automated key generation
  - [ ] RSA
  - [ ] EC
- [ ] Automated Rotation
- [ ] JSON Web Keys
  - [ ] JWK validation
  - [ ] JWK encryption
  - [ ] PEM to JWK
  - [ ] JWK to PEM
  - [ ] JWK Set
- [ ] Serialization of the keychain and all keys
  - [ ] should handle encrypting of private keys as JWE automatically




